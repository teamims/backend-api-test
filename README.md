
### CRUD REST API

## Description

Develop a simple CRUD RESTful API in Typescript or Javascript using a framework and tools of your choice. 

Often when writing backend services, they need to make use of 3rd party API's within our own services/apis. NodeJS is a great tool for aggregating and manipulating data from multiple sources and transforming it for use in a new service.

## Use Case

A 3rd party API holds all of the data required. IMS Require a new microservice api that interacts with said 3rd party API to retrieve data for use in our systems as part of a larger service we wish to offer our client, Acme Inc.

Below outlines the schema of the data stored in the 3rd party service.

### API URL: https://5f8d94a84c15c40016a1de81.mockapi.io/v1

# User

- id: integer;
- name: string;
- department: string;
- specialty: string;

# Task

- id: integer;
- userId: Parent User id;
- task_name: string;
- priority: integer;
- createdAt: Date;

# Relationships
- User has many Tasks
- Tasks belong to one user


### Example urls that provide data

- Retrieve all users: /v1/user
- Retrieve Single user for id: /v1/user/1
- Retrieve Tasks belonging to a user: /v1/user/1/task
- Retrieve Single task belonging to a user: /v1/user/1/task/1
- Ordering Results Example: /v1/user/1/task?sortBy=<field>&order=asc||desc
- Filter results Example: /v1/user/1/task?<field>=<value>

## Business Rules:

- create CRUD endpoints for the User and Task resources.
- Enable basic filtering for common fields found on each resource.

Successful responses should be formatted like the below;

```
{success: true, data: <data goes here>}
```
All error responses should be formatted like the below;
```
{success: false, error: <error message>}
```

## Bonus:
- Create endpoint which provides a count of users in a specified department
- Create an endpoint to return a list of available departments
- Create an endpoint to return a list of user specialties
